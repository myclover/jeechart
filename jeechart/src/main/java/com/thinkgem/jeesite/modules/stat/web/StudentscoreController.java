/**
 * There are <a href="https://github.com/thinkgem/jeesite">JeeSite</a> code generation
 */
package com.thinkgem.jeesite.modules.stat.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.stat.entity.Studentscore;
import com.thinkgem.jeesite.modules.stat.service.StudentscoreService;

/**
 * 学生成绩Controller
 * @author zhangfeng
 * @version 2014-09-18
 */
@Controller
@RequestMapping(value = "${adminPath}/stat/studentscore")
public class StudentscoreController extends BaseController {

	@Autowired
	private StudentscoreService studentscoreService;
	
	@ModelAttribute
	public Studentscore get(@RequestParam(required=false) String id) {
		if (StringUtils.isNotBlank(id)){
			return studentscoreService.get(id);
		}else{
			return new Studentscore();
		}
	}
	
	@RequiresPermissions("stat:studentscore:view")
	@RequestMapping(value = {"list", ""})
	public String list(@RequestParam Map<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<Studentscore> page = studentscoreService.find(new Page<Studentscore>(request, response), paramMap); 
        model.addAttribute("page", page);
		return "modules/" + "stat/studentscoreList";
	}

	@RequiresPermissions("stat:studentscore:view")
	@RequestMapping(value = "form")
	public String form(Studentscore studentscore, Model model) {
		model.addAttribute("studentscore", studentscore);
		return "modules/" + "stat/studentscoreForm";
	}

}
